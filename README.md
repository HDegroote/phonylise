# PHONYLISE
## Phonetic analysis of written text, with a focus on poetry 

Features include:
* Find pronunciations with a given stress pattern
* Analyse the rhyme schema of a poem
* Identify alliterations

Disclaimer: this project is in alfa stage, and the interface is likely to change

This project builds on [phonyface](https://gitlab.com/HDegroote/phonyface), an interface to phonetic dictionaries

### Installation instructions
This code was built with poetry. To install the dependencies:

    poetry install

Alternatively:

    pip install -r requirements.txt

### Examples

    python poem_analysis_example.py
    python stress_analysis_example.py

### Example usage

##### Stress analysis
Get all words which have the same stress pattern as serendipity
    
    cmu_dict = CMUDict()
    reverse_dict = ReversePhoneticDict.init_from_phonetic_dict(cmu_dict)
    serendipity_pronunciation = cmu_dict.get_pronunciation("serendipity")
    print(serendipity_pronunciation, "---> pattern: ", serendipity_pronunciation.stress_pattern)
     ... S EH2 R AH0 N D IH1 P IH0 T IY0  ---> pattern:  20100

    pattern_filter = SameStressPatternAsFilter.init_from_pronunciation(serendipity_pronunciation)
    filtered_pronunciations = pattern_filter.filter(reverse_dict.keys())
    for i, pronunciation in enumerate(filtered_pronunciations, 1):
        word = reverse_dict.get_word(pronunciation)
        print(f"\t{i}) {word} -> {pronunciation} -- {pronunciation.stress_pattern} ")
     ... 1) aberrational -> AE2 B ER0 EY1 SH AH0 N AH0 L  -- 20100 
	     2) abnormalities -> AE2 B N AO0 R M AE1 L AH0 T IY0 Z  -- 20100 
	     3) abnormality -> AE2 B N AO0 R M AE1 L AH0 T IY0  -- 20100 
         ...
         1250) yugoslavian -> Y UW2 G OW0 S L AA1 V IY0 AH0 N  -- 20100 
  



##### Poem analysis
get the rhyme schema of a poem (using 'Spring' by Gerard Manley Hopkins as example)

input a poem

    poem_text = """Nothing is so beautiful as Spring –         
       When weeds, in wheels, shoot long and lovely and lush;         
       Thrush’s eggs look little low heavens, and thrush         
    Through the echoing timber does so rinse and wring         
    The ear, it strikes like lightnings to hear him sing;
       The glassy peartree leaves and blooms, they brush         
       The descending blue; that blue is all in a rush         
    With richness; the racing lambs too have fair their fling.         
    
    What is all this juice and all this joy?         
       A strain of the earth’s sweet being in the beginning
    In Eden garden. – Have, get, before it cloy,         
       Before it cloud, Christ, lord, and sour with sinning,         
    Innocent mind and Mayday in girl and boy,         
       Most, O maid’s child, thy choice and worthy the winning.          
    """

create poem object

    poem = Poem(lines=poem_text.split("\n"))

tokenise poem

    poem_tokeniser = PoemTokeniser()
    tokenised_poem = poem_tokeniser.tokenise_poem(poem)
    
Phonetise poem

    pronounced_poem_creator = PronouncedPoemCreator()  # This loads a phonetic dict, so takes a few seconds
    pronounced_poem = pronounced_poem_creator.create_from(tokenised_poem)
  
analyse poem

    rhyme_scheme_analyser = RhymeSchemaAnalyser()
    rhyme_scheme = rhyme_scheme_analyser.analyse(pronounced_poem)
    print("\nRhyme scheme for Gerard Manley Hopkins's 'Spring':\n ")
    
    for line, rhyme in zip(poem.lines, rhyme_scheme):
        print("\t{0:60}  {1}".format(line, rhyme))

Result: it is an extremely rhymy sonnet

    Nothing is so beautiful as Spring –                           A
       When weeds, in wheels, shoot long and lovely and lush;     B
       Thrush’s eggs look little low heavens, and thrush          B
    Through the echoing timber does so rinse and wring            A
    The ear, it strikes like lightnings to hear him sing;         A
       The glassy peartree leaves and blooms, they brush          B
       The descending blue; that blue is all in a rush            B
    With richness; the racing lambs too have fair their fling.    A
                                                                  
    What is all this juice and all this joy?                      C
       A strain of the earth’s sweet being in the beginning       D
    In Eden garden. – Have, get, before it cloy,                  C
       Before it cloud, Christ, lord, and sour with sinning,      D
    Innocent mind and Mayday in girl and boy,                     C
       Most, O maid’s child, thy choice and worthy the winning.   D

As you can see, this example poem is full of future work in terms of extractable style figures.
Do feel free to contact me if you feel like collaborating on extending this toolkit!
