import unittest

from phonylise.phoneme_filters import PhonemeFilter, AlliterationFilter, EndsWithPhonemesFilter
from phonyface.pronunciation import Pronunciation
from tests.data import RhymeData


class TestPhonemeFilter(unittest.TestCase):
    def setUp(self):
        self.data = RhymeData()
        self.phoneme_pattern_AE_N = self.data.AE.symbol + PhonemeFilter.PHONEME_SYMBOL_SEPARATOR + self.data.N.symbol
        self.phoneme_filter_AE_N = PhonemeFilter(self.phoneme_pattern_AE_N)

    def test_init(self):
        self.assertEqual(self.phoneme_filter_AE_N.pattern, self.phoneme_pattern_AE_N)

    def test_init_fails_if_too_many_phoneme_separators_after_eachother(self):
        pattern = self.phoneme_pattern_AE_N + PhonemeFilter.PHONEME_SYMBOL_SEPARATOR * 2 \
                  + self.phoneme_pattern_AE_N
        with self.assertRaises(ValueError) as context:
            PhonemeFilter(pattern)

    def test_matches_valid(self):
        self.assertTrue(self.phoneme_filter_AE_N.matches(self.data.AE_N_pronunciation))

    def test_matches_invalid_other_pronunciation(self):
        self.assertFalse(self.phoneme_filter_AE_N.matches(self.data.AH_N_pronunciation))

    def test_matches_invalid_only_matches_subset_of_pronunciation_at_start(self):
        self.assertFalse(self.phoneme_filter_AE_N.matches(self.data.AE_N_AH_pronunciation))

    def test_matches_invalid_only_matches_subset_of_pronunciation_at_end(self):
        data = self.data
        N_AE_N_pron = Pronunciation(phonemes=[data.N, data.AE, data.N],
                                    stress_pattern=data.AE_N_pronunciation.stress_pattern)
        self.assertFalse(self.phoneme_filter_AE_N.matches(N_AE_N_pron))


class TestAlliterationFilter(unittest.TestCase):
    def setUp(self):
        self.data = RhymeData()
        self.N_AE_pronunciation = Pronunciation([self.data.N, self.data.AE], stress_pattern=self.data.STRESS_PATTERN_1)

    def test_cannot_init_with_vowel(self):
        with self.assertRaises(ValueError):
            AlliterationFilter(self.data.AE)

    def test_matches_positive(self):
        consonant = self.data.N
        alliteration_filter = AlliterationFilter(consonant)
        self.assertTrue(alliteration_filter.matches(self.N_AE_pronunciation))

    def test_matches_negative(self):
        alliteration_filter = AlliterationFilter(self.data.B)
        self.assertFalse(alliteration_filter.matches(self.N_AE_pronunciation))



class TestEndsWithPhonemesFilter(unittest.TestCase):
    def setUp(self):
        self.data = RhymeData()

    def test_matches_positive(self):
        ana_pron = self.data.AE_N_AH_pronunciation
        filter = EndsWithPhonemesFilter(ana_pron.phonemes[1:])
        self.assertTrue(filter.matches(ana_pron))

    def test_matches_positive_full_match(self):
        ana_pron = self.data.AE_N_AH_pronunciation
        filter = EndsWithPhonemesFilter(ana_pron.phonemes)
        self.assertTrue(filter.matches(ana_pron))

    def test_matches_negative(self):
        ana_pron = self.data.AE_N_AH_pronunciation
        filter = EndsWithPhonemesFilter([self.data.AE, self.data.N])
        self.assertFalse(filter.matches(ana_pron))
