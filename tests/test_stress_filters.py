import unittest

from phonyface.pronunciation import Pronunciation
from phonylise.stress_filters import StressPatternFilter, SameStressPatternAsFilter, \
    EndsWithStressPatternFilter
from tests.data import Data, RhymeData


class TestStressPatternFilter(unittest.TestCase):
    def setUp(self):
        self.pattern = "1"
        self.stress_pattern = StressPatternFilter(self.pattern)

    def test_init(self):
        self.assertEqual(self.stress_pattern.pattern, self.pattern)

    def test_init_with_invalid_chars_fails(self):
        with self.assertRaises(ValueError):
            StressPatternFilter("3")
        with self.assertRaises(ValueError):
            StressPatternFilter("1a")

    def test_init_from_pronunciation_valid(self):
        data = Data()
        pronunciation = data.AE_N_AH_pronunciation
        gold = "10"
        pattern = StressPatternFilter.init_from_pronunciation(pronunciation)
        self.assertEqual(gold, pattern.pattern)

    def test_matches_direct_match(self):
        data = Data()
        pronunciation = data.AE_N_pronunciation
        pattern = StressPatternFilter("1")
        self.assertTrue(pattern.matches(pronunciation))

    def test_matches_non_match(self):
        data = Data()
        pronunciation = data.AE_N_pronunciation
        pattern = StressPatternFilter("00")
        self.assertFalse(pattern.matches(pronunciation))

    def test_matches_does_not_match_when_subset(self):
        data = Data()
        pronunciation = data.AE_N_AH_pronunciation
        pattern = StressPatternFilter("1")
        self.assertFalse(pattern.matches(pronunciation))

    def test_filter(self):
        data = Data()
        pronunciations = {data.AE_N_AH_pronunciation, data.AE_N_AH_pronunciation, data.AH_N_pronunciation}
        pattern = StressPatternFilter("1.*")
        gold = {data.AE_N_AH_pronunciation, data.AE_N_AH_pronunciation}
        self.assertEqual(gold, set(pattern.filter(pronunciations)))

    def test_eq_valid(self):
        p1 = StressPatternFilter("012")
        p2 = StressPatternFilter("012")
        self.assertEqual(p1, p2)

    def test_eq_invalid(self):
        p1 = StressPatternFilter("0")
        p2 = StressPatternFilter("00")
        self.assertNotEqual(p1, p2)

    def test_hash(self):
        p1 = StressPatternFilter("0")
        p2 = StressPatternFilter("0")
        patterns = {p1, p2}
        self.assertEqual(len(patterns), 1)


class TestSameStressPatternAsFilter(unittest.TestCase):
    def setUp(self):
        data = Data()
        self.pronunciation1 = data.AE_N_pronunciation
        self.pronunciation2 = data.AE_N_AH_pronunciation
        self.pronunciations = [self.pronunciation1, self.pronunciation2]
        self.filter = SameStressPatternAsFilter(self.pronunciations)

    def test_matches_positive(self):
        self.assertTrue(self.filter.matches(self.pronunciation1))
        self.assertTrue(self.filter.matches(self.pronunciation2))

    def test_matches_negative(self):
        self.assertFalse(self.filter._matches_str_stress_pattern("0"))
        self.assertFalse(self.filter._matches_str_stress_pattern("11"))
        self.assertFalse(self.filter._matches_str_stress_pattern("2"))


class TestEndsWithStressPatternFilter(unittest.TestCase):
    def setUp(self):
        self.data = RhymeData()
        self.filter_1 = EndsWithStressPatternFilter(self.data.STRESS_PATTERN_1)
        self.filter_10 = EndsWithStressPatternFilter(self.data.STRESS_PATTERN_10)


    def test_matches_positive_1_syllable(self):
        self.assertTrue(self.filter_1.matches(self.data.P_IH1_NG))
        unreal_pronunciation = Pronunciation(self.data.B_AO1_R_IH0_NG.phonemes,
                                             self.data.STRESS_PATTERN_01)
        self.assertTrue(self.filter_1.matches(unreal_pronunciation))

    def test_matches_negative_1_syllable(self):
        self.assertFalse(self.filter_1.matches(self.data.B_AO1_R_IH0_NG))

    def test_matches_positive_2_syllables(self):
        self.assertTrue(self.filter_10.matches(self.data.B_AO1_R_IH0_NG))
        unreal_pronunciation = Pronunciation([self.data.AH, *self.data.B_AO1_R_IH0_NG.phonemes],
                                             self.data.STRESS_PATTERN_010)
        self.assertTrue(self.filter_10.matches(unreal_pronunciation))

    def test_matches_negative_2_syllables(self):
        self.assertFalse(self.filter_10.matches(self.data.AH_N_pronunciation))
        unreal_pronunciation = Pronunciation(self.data.B_AO1_R_IH0_NG.phonemes,
                                             self.data.STRESS_PATTERN_01)
        self.assertFalse(self.filter_10.matches(unreal_pronunciation))


if __name__ == '__main__':
    unittest.main()