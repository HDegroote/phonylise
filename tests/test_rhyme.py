import unittest

from phonylise.rhyme import PerfectRhymeFilter
from tests.data import RhymeData


class TestPerfectRhymeFilter(unittest.TestCase):
    def setUp(self):
        self.data = RhymeData()
        self.ring_filter = PerfectRhymeFilter(self.data.R_IH1_NG)
        self.boring_filter = PerfectRhymeFilter(self.data.B_AO1_R_IH0_NG)

    def test_init_pronunciation_without_main_stress(self):
        AH0_N_filter = PerfectRhymeFilter(self.data.AH_N_pronunciation)
        self.assertTrue(AH0_N_filter.matches(self.data.AH_N_pronunciation))

    def test_init_pronunciation_with_more_than_one_main_stress(self):
        peking_filter = PerfectRhymeFilter(self.data.P_IY1_K_IH1_NG)
        self.assertTrue(peking_filter.matches(self.data.P_IY1_K_IH1_NG))
        self.assertTrue(peking_filter.matches(self.data.R_IH1_NG))

    def test_matches_positive(self):
        self.assertTrue(self.ring_filter.matches(self.data.R_IH1_NG))
        self.assertTrue(self.ring_filter.matches(self.data.P_IH1_NG))

    def test_matches_negative(self):
        self.assertFalse(self.ring_filter.matches(self.data.B_AO1_R_IH0_NG))
        self.assertFalse(self.ring_filter.matches(self.data.AE_N_pronunciation))

    def test_feminine_rhyme_positive(self):
        self.assertTrue(self.boring_filter.matches(self.data.B_AO1_R_IH0_NG))
        self.assertTrue(self.boring_filter.matches(self.data.R_AO1_R_IH0_NG))

    def test_feminine_rhyme_negative(self):
        self.assertFalse(self.boring_filter.matches(self.data.R_IH1_NG))
        self.assertFalse(self.boring_filter.matches(self.data.AE_N_AH_pronunciation))


if __name__ == '__main__':
    unittest.main()
