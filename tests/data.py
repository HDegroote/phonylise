import copy

from phonyface.phoneme import Phoneme, PhonemeType
from phonyface.phonetic_dict import PhoneticDict, ReversePhoneticDict
from phonylise.poem_analysis import TokenisedPoem, PronouncedPoem
from phonyface.pronunciation import Pronunciation, Stress, StressPattern, UnknownPronunciation


class Data:
    def __init__(self):
        """This data can be used for unit tests.
        Do make sure to never share the same Data object between different unit tests (do not define it in the setUp)
        """
        # DEV NOTE: making changes here might break a lot of tests if you're not careful
        self.STRESS_PATTERN_1 = StressPattern((Stress.MAIN,))
        self.AE = Phoneme("AE", PhonemeType.VOWEL)
        self.AH = Phoneme("AH", PhonemeType.VOWEL)
        self.N = Phoneme("N", PhonemeType.NASAL)
        self.an_word = "an"
        self.AE_N_phonemes = [self.AE, self.N]
        self.AE_N_stresses = self.STRESS_PATTERN_1
        self.AE_N_pronunciation = Pronunciation(phonemes=self.AE_N_phonemes,
                                                stress_pattern=self.AE_N_stresses)
        self.AH_N_phonemes = [self.AH, self.N]
        self.AH_N_stresses = (Stress.NONE,)
        self.AH_N_pronunciation = Pronunciation(phonemes=self.AH_N_phonemes,
                                                stress_pattern=self.AH_N_stresses)
        self.an_entry = (self.an_word, (self.AE_N_pronunciation, self.AH_N_pronunciation))

        self.ana_word = "ana"
        self.AE_N_AH_phonemes = [self.AE, self.N, self.AH]
        self.AE_N_AH_stresses = (Stress.MAIN, Stress.NONE)
        self.AE_N_AH_pronunciation = Pronunciation(phonemes=self.AE_N_AH_phonemes,
                                                   stress_pattern=self.AE_N_AH_stresses)
        self.ana_entry = (self.ana_word, (self.AE_N_AH_pronunciation,))
        self.an_ana_dict_content = dict([self.ana_entry, self.an_entry])
        self.an_ana_dict = PhoneticDict(self.an_ana_dict_content)

        self.ann_word = "ann"
        self.AE_N_reverse_entry = (self.AE_N_pronunciation, (self.an_word, self.ann_word))
        self.an_ann_reverse_dict = ReversePhoneticDict(dict([self.AE_N_reverse_entry]))


class RhymeData(Data):

    def __init__(self):
        super().__init__()

        self.B = Phoneme("B", PhonemeType.STOP)
        self.STRESS_PATTERN_01 = StressPattern((Stress.NONE, Stress.MAIN))
        self.STRESS_PATTERN_010 = StressPattern((Stress.NONE, Stress.MAIN, Stress.NONE))
        self.STRESS_PATTERN_11 = StressPattern((Stress.MAIN, Stress.MAIN))
        self.STRESS_PATTERN_10 = StressPattern((Stress.MAIN,Stress.NONE))

        self.P = Phoneme("P", PhonemeType.STOP)
        self.IH = Phoneme("IH", PhonemeType.VOWEL)
        self.NG = Phoneme("NG", PhonemeType.NASAL)
        self.P_IH1_NG = Pronunciation(phonemes=[self.P, self.IH, self.NG],
                                      stress_pattern=self.STRESS_PATTERN_1)
        self.ping = "ping"

        self.R = Phoneme("R", PhonemeType.LIQUID)
        self.R_IH1_NG = Pronunciation(phonemes=[self.R, self.IH, self.NG],
                                      stress_pattern=self.STRESS_PATTERN_1)
        self.ring = "ring"

        self.AO = Phoneme("AO", PhonemeType.VOWEL)
        self.B_AO1_R_IH0_NG = Pronunciation(phonemes=[self.B, self.AO, self.R, self.IH, self.NG],
                                            stress_pattern=self.STRESS_PATTERN_10)
        self.boring = "boring"

        self.R_AO1_R_IH0_NG = Pronunciation(phonemes=[self.R, self.AO, self.R, self.IH, self.NG],
                                            stress_pattern=self.STRESS_PATTERN_10)
        self.roaring = "roaring"

        self.IH = Phoneme("IH", PhonemeType.VOWEL)
        self.IY = Phoneme("IY", PhonemeType.VOWEL)
        self.K = Phoneme("K", PhonemeType.STOP)
        self.P_IY1_K_IH1_NG = Pronunciation(phonemes=[self.P, self.IY, self.K, self.IH, self.NG],
                                            stress_pattern=self.STRESS_PATTERN_11)
        self.peking = "peking"

        self.boring_dict = PhoneticDict({})
        self.boring_dict[self.boring] = [self.B_AO1_R_IH0_NG]
        self.boring_dict[self.ping] = [self.P_IH1_NG]
        self.boring_dict[self.ann_word] = [self.AE_N_pronunciation]
        self.boring_dict[self.ring] = [self.R_IH1_NG]
        self.boring_dict[self.peking] = [self.P_IY1_K_IH1_NG]
        self.boring_dict[self.roaring] = [self.R_AO1_R_IH0_NG]
        self.boring_dict[self.an_entry[0]] = self.an_entry[1]
        self.ABBA_tokenised_poem = TokenisedPoem([
            ["boring"],
            ["ping"],
            ["ann", "ring"],
            ["peking", "roaring"]
        ])
        __abba_pronunciations = [
            [self.boring_dict.get_pronunciation(word) for word in poem_line] for poem_line in self.ABBA_tokenised_poem.lines
        ]
        self.ABBA_pronounced_poem = PronouncedPoem(__abba_pronunciations)

        self.boring_dict_sin_roaring = copy.deepcopy(self.boring_dict)
        self.boring_dict_sin_roaring.pop("roaring")
        assert len(self.boring_dict_sin_roaring) == len(self.boring_dict) - 1
        __abb_unk_pronunciations = [
            [self.boring_dict_sin_roaring.get_pronunciation(word, default=UnknownPronunciation())
             for word in poem_line]
            for poem_line in self.ABBA_tokenised_poem.lines
        ]
        self.ABB_UNK_pronounced_poem = PronouncedPoem(__abb_unk_pronunciations)

