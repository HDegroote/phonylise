import copy
import unittest
from typing import Sequence, MutableSequence

from phonylise import poem_analysis
from phonylise.poem_analysis import Poem, TokenisedPoem, RhymeSchemaAnalyser, PronouncedPoem, PronouncedPoemCreator
from tests.data import RhymeData


class TestPoem(unittest.TestCase):
    def setUp(self):
        self.lines = ["I am a line", "I am a line two"]
        self.poem = Poem(self.lines)

    def test_deepcopy_init(self):
        old_lines = copy.deepcopy(self.lines)
        self.lines.remove(self.lines[1])
        self.assertNotEqual(old_lines, self.lines)
        self.assertEqual(tuple(old_lines), tuple(self.poem.lines))

    def test_immutable_lines(self):
        self.assertIsInstance(self.poem.lines, Sequence)
        self.assertNotIsInstance(self.poem.lines, MutableSequence)

# ToDo Decide on how to test (with spacy tokeniser, or abstract tokeniser used?)

# class TestTokenisedPoem(unittest.TestCase):
#     def setUp(self):
#         self.lines = [["I", "rhyme"], ["With", "line"]]
#         self.poem = TokenisedPoem(self.lines)
#
#     def test_deep_copy_init(self):
#         old_lines = copy.deepcopy(self.lines)
#         self.lines.remove(self.lines[0])
#         self.assertNotEqual(len(old_lines), len(self.lines))
#         self.assertEqual(len(old_lines), len(self.poem.lines))
#
#     def test_deep_deep_copy_init(self):
#         old_lines = copy.deepcopy(self.lines)
#         self.lines[0].remove(self.lines[0][0])
#         self.assertNotEqual(len(old_lines[0]), len(self.lines[0]))
#         self.assertEqual(len(old_lines[0]), len(self.poem.lines[0]))
#
#     def test_immutable_lines(self):
#         self.assertIsInstance(self.poem.lines, Sequence)
#         self.assertNotIsInstance(self.poem.lines, MutableSequence)
#
#         self.assertIsInstance(self.poem.lines[0], Sequence)
#         self.assertNotIsInstance(self.poem.lines[0], MutableSequence)
#
#
# class TestPronouncedPoem(unittest.TestCase):
#     def setUp(self):
#         data = RhymeData()
#         self.lines = [[data.AE_N_pronunciation, data.AH_N_pronunciation],
#                       [data.AH_N_pronunciation, data.AE_N_pronunciation]]
#         self.poem = PronouncedPoem(self.lines)
#
#     def test_init(self):
#         for line_gold, line_poem in zip(self.lines, self.poem.lines):
#             for pron_gold, pron_poem in zip(line_gold, line_poem):
#                 self.assertEqual(pron_gold, pron_poem)
#
#     def test_deep_copy_init(self):
#         old_lines = copy.deepcopy(self.lines)
#         self.lines.remove(self.lines[0])
#         self.assertNotEqual(len(old_lines), len(self.lines))
#         self.assertEqual(len(old_lines), len(self.poem.lines))
#
#     def test_deep_deep_copy_init(self):
#         old_lines = copy.deepcopy(self.lines)
#         self.lines[0].remove(self.lines[0][0])
#         self.assertNotEqual(len(old_lines[0]), len(self.lines[0]))
#         self.assertEqual(len(old_lines[0]), len(self.poem.lines[0]))
#
#     def test_immutable_lines(self):
#         self.assertIsInstance(self.poem.lines, Sequence)
#         self.assertNotIsInstance(self.poem.lines, MutableSequence)
#         self.assertIsInstance(self.poem.lines[0], Sequence)
#         self.assertNotIsInstance(self.poem.lines[0], MutableSequence)
#
#
# class TestRhymeSchemaAnalyser(unittest.TestCase):
#     def test_analyse_simple_ABBA(self):
#         data = RhymeData()
#         gold = ["A", "B", "B", "A"]
#         analyser = RhymeSchemaAnalyser()
#         analysis = analyser.analyse(data.ABBA_pronounced_poem)
#         self.assertEqual(analysis, gold)
#
#     def test_analyse_with_unknowns(self):
#         data = RhymeData()
#         gold = ["A", "B", "B", "?"]
#         analyser = RhymeSchemaAnalyser()
#         analysis = analyser.analyse(data.ABB_UNK_pronounced_poem)
#         self.assertEqual(analysis, gold)
#
#
# class TestPronouncedPoemCreator(unittest.TestCase):
#
#     def test_create_poem(self):
#         data = RhymeData()
#         gold = data.ABBA_pronounced_poem
#         poem_creator = PronouncedPoemCreator(data.boring_dict)
#         created_pronounced_poem = poem_creator.create_from(data.ABBA_tokenised_poem)
#         self.assertEqual(created_pronounced_poem.lines, gold.lines)
#
#     def test_create_poem_with_unknown_pronunciations(self):
#         data = RhymeData()
#         gold = data.ABB_UNK_pronounced_poem
#         poem_creator = PronouncedPoemCreator(data.boring_dict_sin_roaring)
#         created_pronounced_poem = poem_creator.create_from(data.ABBA_tokenised_poem)
#         self.assertEqual(created_pronounced_poem.lines, gold.lines)
#
# class TestLooseFunctions(unittest.TestCase):
#
#     def test_tokenise_poem(self):
#         poem = Poem(["I am a poem", "How about you?"])
#         gold_tokenisation = [["I", "am", "a", "poem"], ["How", "about", "you?"]]
#         gold_poem = PronouncedPoem(gold_tokenisation)
#         tokenised_poem = poem_analysis.tokenise_poem(poem)
#         self.assertEqual(gold_poem.lines, tokenised_poem.lines)
if __name__ == '__main__':
    unittest.main()
