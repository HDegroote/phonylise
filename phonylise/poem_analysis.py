from typing import Sequence, Tuple, List, NamedTuple
from phonyface.interfaces.mixed_cmu_g2p_interface import CmuDictWithG2pFallback
from phonyface.phonetic_dict import PhoneticDict, PhoneticDictMixin
from phonyface.pronunciation import Pronunciation, UnknownPronunciation
from spacy.lang.en import English
from spacy.tokens import Token

from phonylise.rhyme import PerfectRhymeFilter


class Poem:
    def __init__(self, lines: Sequence[str]):
        self.__lines: Tuple[str] = self.clean_lines(lines)

    def get_lines(self) -> Tuple[str]:
        return self.__lines

    lines = property(get_lines)

    def __str__(self):
        return "\n".join(self.lines)

    def __repr__(self):
        return "Poem object with lines:\n" + str(self)

    @classmethod
    def clean_lines(cls, lines: Sequence[str]) -> Tuple[str]:
        return tuple(line.rstrip() for line in lines)


class TokenisedPoem:
    def __init__(self, lines: Sequence[Sequence[Token]]):
        self.__lines: Tuple[Tuple[Token]] = tuple(
            tuple(
                token for token in tokens_in_line
            ) for tokens_in_line in lines
        )

    def get_lines(self):
        return self.__lines

    lines = property(get_lines)

    def __str__(self):
        res = ""
        for line in self.lines:
            res += " ".join([token.text for token in line]) + "\n"
        return res

    def __repr__(self):
        return "Tokenised poem object with lines:\n" + str(self)


class PronouncedPoem:
    def __init__(self, lines: Sequence[Sequence[Pronunciation]]):
        self.__lines: Tuple[Tuple[Pronunciation]] = tuple(
            tuple(
                pronunciation for pronunciation in pronunciations_in_line
            ) for pronunciations_in_line in lines
        )

    def get_lines(self) -> Tuple[Tuple[Pronunciation]]:
        return self.__lines

    lines = property(get_lines)

    def __str__(self):
        res = ""
        for line in self.lines:
            res += " ".join(str(pronunciation) for pronunciation in line) + "\n"
        return res


class PoemTokeniser:
    def __init__(self, nlp=None):
        if not nlp:
            nlp = English()
        self.nlp = nlp

    def tokenise_poem(self, poem: Poem) -> TokenisedPoem:
        tokenised_lines = list(self.nlp.pipe(poem.lines))
        return TokenisedPoem(tokenised_lines)


class RhymeSchemaAnalyser:
    UNKNOWN_SYMBOL = "?"

    @classmethod
    def analyse(cls, pronounced_poem: PronouncedPoem) -> List[str]:
        unknown_index = -1
        empty_line_index = -2
        assert unknown_index < 0
        assert empty_line_index < 0
        assert empty_line_index != unknown_index

        last_pronunciations = [cls._get_last_pronunciation(line) for line in pronounced_poem.lines]
        rhyme_schema: List[int] = []
        rhyme_filters: List[PerfectRhymeFilter] = []
        for pronunciation in last_pronunciations:
            # ToDo clean up handling of unknown pronunciation and empty lines
            if pronunciation is None:
                rhyme_schema.append(empty_line_index)
            elif pronunciation == UnknownPronunciation():
                rhyme_schema.append(unknown_index)
            else:
                rhyme_found = False
                for rhyme_filter_index, rhyme_filter in enumerate(rhyme_filters):
                    if rhyme_filter.matches(pronunciation):
                        rhyme_schema.append(rhyme_filter_index)
                        rhyme_found = True
                        break
                if not rhyme_found:
                    new_rhyme_filter = PerfectRhymeFilter(pronunciation)
                    rhyme_filters.append(new_rhyme_filter)
                    rhyme_schema.append(len(rhyme_filters) - 1)

        rhyme_schema_with_char_notation = [chr(ord("A") + rhyme_index)
                                           if rhyme_index >= 0 else
                                           cls.UNKNOWN_SYMBOL if rhyme_index == unknown_index
                                           else ""
                                           for rhyme_index in rhyme_schema]
        return rhyme_schema_with_char_notation

    @classmethod
    def _get_last_pronunciation(self, line):
        # DEVNOTE: had test here before, but decided not to use it in the end (was for punctuation handling)
        for pronunciation in reversed(line):
            return pronunciation
        return None


class PronouncedPoemCreator:
    def __init__(self, phonetic_dict: PhoneticDictMixin = None):
        if not phonetic_dict:
            phonetic_dict = CmuDictWithG2pFallback()
        self.phonetic_dict = phonetic_dict

    def create_from(self, tokenised_poem: TokenisedPoem) -> PronouncedPoem:
        pronounced_lines = list()
        for tokenised_line in tokenised_poem.lines:
            pronounced_line = []
            for token in tokenised_line:
                if not token.is_punct:
                    pron = self.phonetic_dict.get_pronunciation(token.text, default=UnknownPronunciation())
                    pronounced_line.append(pron)
                #else:
                #    pronounced_line.append(PunctuationPlaceholder)
            pronounced_lines.append(pronounced_line)
        return PronouncedPoem(pronounced_lines)


