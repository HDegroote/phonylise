from typing import Iterable

from phonylise.filter import Filter
from phonyface.pronunciation import Pronunciation, StressPattern
import regex


class StressPatternFilter(Filter):
    __ALLOWED_PATTERN_CHARS: str = r"012.^$*+?{}[]\|()"
    __PATTERN_PRE_PADDING: str = "("
    __PATTERN_POST_PADDING: str = ")$"

    def __init__(self, pattern: str):
        # DEV NOTE: this test is not complete, and regexes can easily be invalid (error thrown when applying)
        for char in pattern:
            if char not in self.__ALLOWED_PATTERN_CHARS:
                raise ValueError(f"The pattern '{pattern}' contains an illegal character: '{char}' "
                                 f"and makes no sense for matching of stress patterns")
        self.__pattern = pattern

    @classmethod
    def init_from_pronunciation(cls, pronunciation: Pronunciation) -> "StressPatternFilter":
        return cls(str(pronunciation.stress_pattern))

    @classmethod
    def get_pre_padding(cls) -> str:
        return cls.__PATTERN_PRE_PADDING

    @classmethod
    def get_post_padding(cls) -> str:
        return cls.__PATTERN_POST_PADDING

    @classmethod
    def pad_pattern(cls, pattern: str) -> str:
        return cls.__PATTERN_PRE_PADDING + pattern + cls.__PATTERN_POST_PADDING

    def get_pattern(self) -> str:
        return self.__pattern

    pattern = property(get_pattern)

    def matches(self, pronunciation: Pronunciation):
        str_stress_pattern = str(pronunciation.stress_pattern)
        return self._matches_str_stress_pattern(str_stress_pattern)

    def _matches_str_stress_pattern(self, str_stress_pattern: str) -> bool:
        return regex.match(self.pad_pattern(self.pattern), str_stress_pattern) is not None

    def __str__(self):
        return self.pattern

    def __repr__(self):
        return "StressPatternFilter object " + str(self)

    def __eq__(self, other) -> bool:
        if isinstance(other, StressPatternFilter):
            if self.pattern == other.pattern:
                return True
        return NotImplemented

    def __hash__(self):
        return hash(self.pattern)


class SameStressPatternAsFilter(StressPatternFilter):

    def __init__(self, pronunciations: Iterable[Pronunciation]):
        all_stress_patterns = {"(" + str(pronunciation.stress_pattern) + ")" for pronunciation in pronunciations}
        overall_pattern = "|".join(all_stress_patterns)
        super().__init__(overall_pattern)

    @classmethod
    def init_from_pronunciation(cls, pronunciation: Pronunciation):
        return cls([pronunciation])


class EndsWithStressPatternFilter(StressPatternFilter):
    def __init__(self, stress_pattern: StressPattern):
        pattern = ".*" + str(stress_pattern)
        super().__init__(pattern)
