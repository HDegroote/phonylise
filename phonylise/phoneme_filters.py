from typing import Sequence

from regex import regex
from phonylise.filter import Filter
from phonyface.phoneme import Phoneme
from phonyface.pronunciation import Pronunciation


class PhonemeFilter(Filter):
    PHONEME_SYMBOL_SEPARATOR = " "
    __PATTERN_POST_PADDING = "$"

    def __init__(self, pattern: str):
        double_sep_index = pattern.find(self.PHONEME_SYMBOL_SEPARATOR * 2)
        if double_sep_index >= 0:
            raise ValueError("The pattern contains two phoneme symbol separators after each other at index "
                             f"{double_sep_index}")
        self.__pattern: str = pattern

    def get_pattern(self) -> str:
        return self.__pattern

    pattern = property(get_pattern)

    @classmethod
    def get_pattern_post_padding(cls):
        return cls.__PATTERN_POST_PADDING

    def matches(self, pronunciation: Pronunciation) -> bool:
        phonemes_as_str = self._separate_phoneme_symbols_by_sep_in_str(pronunciation.phonemes)
        return self._matches_str_phonemes(phonemes_as_str)

    def _matches_str_phonemes(self, str_phonemes: str) -> bool:
        padded_pattern = self.pattern + self.get_pattern_post_padding()
        match = regex.match(padded_pattern, str_phonemes)
        return match is not None

    @classmethod
    def _separate_phoneme_symbols_by_sep_in_str(cls, phonemes: Sequence[Phoneme]) -> str:
        phoneme_symbols = [phoneme.symbol for phoneme in phonemes]
        phonemes_as_str = cls.PHONEME_SYMBOL_SEPARATOR.join(phoneme_symbols)
        return phonemes_as_str


class AlliterationFilter(PhonemeFilter):
    def __init__(self, consonant: Phoneme):
        if consonant.is_vowel():
            raise ValueError("Cannot create an alliteration filter with a vowel phoneme")
        pattern = consonant.symbol + ".*"
        super().__init__(pattern)


class EndsWithPhonemesFilter(PhonemeFilter):
    def __init__(self, phonemes: Sequence[Phoneme]):
        end_of_pattern = self._separate_phoneme_symbols_by_sep_in_str(phonemes)
        pattern = ".*" + end_of_pattern
        super().__init__(pattern)
