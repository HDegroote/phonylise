import abc
from collections import Iterable
from typing import List

from phonyface.pronunciation import Pronunciation


class Filter(abc.ABC):

    @abc.abstractmethod
    def matches(self, pronunciation: Pronunciation) -> bool:
        pass

    def filter(self, pronunciations: Iterable, max_matches=float("inf")) -> List[Pronunciation]:
        # ToDo Consider returning iterable and using built-in python filter()
        matches = []
        for pronunciation in pronunciations:
            if self.matches(pronunciation):
                matches.append(pronunciation)
                if len(matches) >= max_matches:
                    break
        return matches
