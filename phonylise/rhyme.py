from phonyface.pronunciation import Pronunciation, Stress

from phonylise.filter import Filter
from phonylise.phoneme_filters import EndsWithPhonemesFilter
from phonylise.stress_filters import EndsWithStressPatternFilter


class PerfectRhymeFilter(Filter):
    def __init__(self, pronunciation: Pronunciation):
        index_of_main_stresses_in_vowel_indices = [i for i, stress in enumerate(pronunciation.stress_pattern)
                                                   if stress == Stress.MAIN]
        if len(index_of_main_stresses_in_vowel_indices) > 0:
            index_of_last_main_stress_in_vowel_indices = index_of_main_stresses_in_vowel_indices[-1]
            index_of_last_stressed_vowel = pronunciation.vowel_indices[index_of_last_main_stress_in_vowel_indices]
            self.to_rhyme_phonemes = pronunciation.phonemes[index_of_last_stressed_vowel:]
            self.end_pattern_to_match = pronunciation.stress_pattern[index_of_last_main_stress_in_vowel_indices:]
        else:  # Not a single stressed vowel
            self.to_rhyme_phonemes = pronunciation.phonemes
            self.end_pattern_to_match = pronunciation.stress_pattern

        self.ends_with_phonemes_filter = EndsWithPhonemesFilter(self.to_rhyme_phonemes)
        self.end_pattern_to_match_filter = EndsWithStressPatternFilter(self.end_pattern_to_match)

    def matches(self, pronunciation: Pronunciation):
        for filter_ in [self.ends_with_phonemes_filter, self.end_pattern_to_match_filter]:
            if not filter_.matches(pronunciation):
                return False
        return True