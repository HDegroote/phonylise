from phonylise.poem_analysis import Poem, PronouncedPoemCreator, RhymeSchemaAnalyser, PoemTokeniser

print("Feel free to insert any poem of your liking, but as a basic example,"
      " let's analyse 'Spring' by Gerard Manley Hopkins, a master of pretty language:")

poem_text = """Nothing is so beautiful as Spring –         
   When weeds, in wheels, shoot long and lovely and lush;         
   Thrush’s eggs look little low heavens, and thrush         
Through the echoing timber does so rinse and wring         
The ear, it strikes like lightnings to hear him sing;
   The glassy peartree leaves and blooms, they brush         
   The descending blue; that blue is all in a rush         
With richness; the racing lambs too have fair their fling.         

What is all this juice and all this joy?         
   A strain of the earth’s sweet being in the beginning
In Eden garden. – Have, get, before it cloy,         
   Before it cloud, Christ, lord, and sour with sinning,         
Innocent mind and Mayday in girl and boy,         
   Most, O maid’s child, thy choice and worthy the winning.          
"""
poem = Poem(lines=poem_text.split("\n"))

# Tokenise it
poem_tokeniser = PoemTokeniser()
tokenised_poem = poem_tokeniser.tokenise_poem(poem)

# Phonetise it
pronounced_poem_creator = PronouncedPoemCreator()  # This loads a phonetic dict, so takes a few seconds
pronounced_poem = pronounced_poem_creator.create_from(tokenised_poem)

# Analyse it
rhyme_scheme_analyser = RhymeSchemaAnalyser()
rhyme_scheme = rhyme_scheme_analyser.analyse(pronounced_poem)
print("\nRhyme scheme for Gerard Manley Hopkins's 'Spring':\n ")

for line, rhyme in zip(poem.lines, rhyme_scheme):
    print("\t{0:60}  {1}".format(line, rhyme))

