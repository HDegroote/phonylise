import itertools
from phonyface.interfaces.cmu_interface import CMUDict
from phonyface.phonetic_dict import ReversePhoneticDict
from phonyface.pronunciation import Stress
from phonylise.stress_filters import StressPatternFilter, SameStressPatternAsFilter

print("Loading dictionary (this can take a few seconds)")
cmu_dict = CMUDict()
reverse_dict = ReversePhoneticDict.init_from_phonetic_dict(cmu_dict)

print(f"\n\n{'*' * 20}WORKING WITH STRESS{'*' * 20}\n")
print("(A simple example of an analysis)")
print("Available stresses:")
for stress in Stress:
    print("\t", stress, "->", stress.value)

print("\nGet the stress pattern of pattern")
stress_pattern_pattern = cmu_dict.get_pronunciation("pattern").stress_pattern
print(f"\tThe stress pattern of pattern is: {stress_pattern_pattern}")

print("\nCheck if a lantern can have the same stress pattern as pattern")
pattern_filter = StressPatternFilter(str(stress_pattern_pattern))
lantern_pronunciations = cmu_dict.get_pronunciations("lantern")
print(f"\t(Lantern only has {len(lantern_pronunciations)} pronunciation, which makes it easier)")
lantern_pronunciation = lantern_pronunciations[0]
is_match = pattern_filter.matches(lantern_pronunciation)
print(f"\tDoes lantern have the same stress pattern as pattern? => {is_match}")

print("\nFind a word with the same stress pattern as serendipity")
serendipity_pronunciations = cmu_dict.get_pronunciations("serendipity")
print(f"Serendipity has only {len(serendipity_pronunciations)} pronunciation: {serendipity_pronunciations[0]}")
serendipity_pronunciation = serendipity_pronunciations[0]
print(f"The stress pattern of {serendipity_pronunciation} is : {serendipity_pronunciation.stress_pattern}")
pattern_filter = SameStressPatternAsFilter.init_from_pronunciation(serendipity_pronunciation)
filtered_pronunciations = pattern_filter.filter(reverse_dict.keys())
# In the unlikely case there exist two or more five-syllable words with this stress pattern with the same pronunciation
filtered_words = itertools.chain(*[reverse_dict.get_words(pronunciation)
                                   for pronunciation in filtered_pronunciations])
print(f"There are {len(filtered_pronunciations)} words which can have the same stress pattern as serendipity. "
      f"\nThree of them are: ")
for i, word in enumerate(filtered_words, 1):
    pronunciation = cmu_dict.get_pronunciation(word)
    stress_pattern = pronunciation.stress_pattern
    print(f"\t{i}) {word} -> {pronunciation} -- {stress_pattern} ")
    if i >= 3:
        break
